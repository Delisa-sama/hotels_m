#ifndef ADDRESERVATIONDIALOG_H
#define ADDRESERVATIONDIALOG_H

#include <QDialog>
#include <QtSql>
#include <QListWidgetItem>

namespace Ui {
class addReservationDialog;
}

class addReservationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addReservationDialog(QWidget *parent = 0);
    ~addReservationDialog();
    void init(int r_num, int h_num, int r_count, int p_count, int f_num, int cpd, QString h_name, QString r_type, int usernum);

private slots:
    void on_buttonBox_accepted();

    void on_servicesListWidget_itemChanged(QListWidgetItem *item);

    void on_servicesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_beginDateEdit_userDateChanged(const QDate &date);

    void on_endDateEdit_userDateChanged(const QDate &date);

private:
    int room_num;
    int hotel_num;
    int floor_num;
    int rooms_count;
    int persons_count;
    int cost;
    int cost_per_day;
    int reservation_num;
    int user_number;
    Ui::addReservationDialog *ui;
    bool isInited = false;
};

#endif // ADDRESERVATIONDIALOG_H
