#include "addreservationdialog.h"
#include "ui_addreservationdialog.h"

addReservationDialog::addReservationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addReservationDialog)
{
    ui->setupUi(this);
}

addReservationDialog::~addReservationDialog()
{
    delete ui;
}

void addReservationDialog::on_buttonBox_accepted()
{
    // Add reservation
    QSqlQuery q;
    q.setForwardOnly(true);

    q.prepare("SELECT * FROM add_reservation(:_client_number, :_room_number, :_hotel_number, :_begin_date, :_end_date)");

    q.bindValue(":_client_number", user_number, QSql::In);
    q.bindValue(":_room_number", room_num, QSql::In);
    q.bindValue(":_hotel_number", hotel_num, QSql::In);
    q.bindValue(":_begin_date", ui->beginDateEdit->date().toString("yyyy-MM-dd"), QSql::In);
    q.bindValue(":_end_date",  ui->endDateEdit->date().toString("yyyy-MM-dd"), QSql::In);

     qDebug() << "PREPARED";

    if (q.exec())
    {
        q.first();
        reservation_num = q.value(0).toInt();
        qDebug() << "EXECUTED: " << reservation_num;
    }
    else {
        qDebug() << q.lastError();
        return;
    }
    // Add services

    qDebug() << "ADDING SERVICES";
    for (int i = 0; i < ui->servicesListWidget->count(); i++)
    {
        QListWidgetItem *item = ui->servicesListWidget->item(i);
        if (item->checkState() == Qt::Checked) {
            QSqlQuery q;
            q.prepare("SELECT * FROM add_service(:reservation_num, :service_name)");
            q.bindValue(":reservation_num", reservation_num, QSql::In);
            q.bindValue(":service_name", item->text(), QSql::In);

            qDebug() << "ADD SERVICE PREPARED: " << item->text();
            if (!q.exec())
                qDebug() << "Add services error: " << q.lastError();
        }
    }

}

void addReservationDialog::init(int r_num, int h_num, int r_count, int p_count, int f_num, int cpd, QString h_name, QString r_type, int usernum)
{

    room_num = r_num;           ui->roomLabel->setText(QString::number(room_num));
    hotel_num = h_num;          ui->hotelLabel->setText(h_name);
    rooms_count = r_count;      ui->roomsCountLabel->setText(QString::number(rooms_count));
    persons_count = p_count;    ui->personsCountLabel->setText(QString::number(persons_count));
    floor_num = f_num;          ui->floorLabel->setText(QString::number(floor_num));
    reservation_num = 0;        ui->roomTypeLabel->setText(r_type);
    cost_per_day = cpd;         ui->costPerDayLabel->setText(QString::number(cost_per_day));
    user_number = usernum;

    QSqlQuery q;
    q.prepare("SELECT * FROM services");
    if (!q.exec())
        qDebug() << q.lastError();

    ui->servicesListWidget->clear();

    while(q.next()) {
        QListWidgetItem* item = new QListWidgetItem(q.value("service_name").toString(), ui->servicesListWidget);
        item->setText(q.value("service_name").toString());
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);

        item->setCheckState(Qt::Unchecked);
        ui->servicesListWidget->addItem(item);
    }

    isInited = true;

    cost = cost_per_day * (ui->beginDateEdit->date().daysTo(ui->endDateEdit->date()));
    ui->costLabel->setText(QString::number(cost));
}


void addReservationDialog::on_servicesListWidget_itemChanged(QListWidgetItem *item)
{

    QSqlQuery q;
    //q.setForwardOnly(true);
    q.prepare("SELECT * FROM services_cost_v WHERE service_name = :serv_name");
    q.bindValue(":serv_name", item->text(), QSql::In);

    if (!q.exec())
        qDebug() << q.lastError();

    if (isInited)
        while(q.next())
            if (item->checkState() == Qt::Unchecked)
                cost -= q.value("service_cost").toInt();
            else
                cost += q.value("service_cost").toInt();

    ui->costLabel->setText(QString::number(cost));
}

void addReservationDialog::on_servicesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    QSqlQuery q;
    //q.setForwardOnly(true);
    q.prepare("SELECT * FROM services_cost_v WHERE service_name = :serv_name");
    q.bindValue(":serv_name", current->text(), QSql::In);

    if (!q.exec())
        qDebug() << q.lastError();

    q.next();

    ui->serviceDiscTextBrowser->setText(q.value("service_type_name").toString());
}

void addReservationDialog::on_beginDateEdit_userDateChanged(const QDate &date)
{
    cost = cost_per_day * (ui->beginDateEdit->date().daysTo(ui->endDateEdit->date()));
    ui->costLabel->setText(QString::number(cost));
}

void addReservationDialog::on_endDateEdit_userDateChanged(const QDate &date)
{
    cost = cost_per_day * (ui->beginDateEdit->date().daysTo(ui->endDateEdit->date()));
    ui->costLabel->setText(QString::number(cost));
}
