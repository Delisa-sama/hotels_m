#include "registrationwindow.h"
#include "ui_registrationwindow.h"

registrationwindow::registrationwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registrationwindow)
{
    ui->setupUi(this);
}

registrationwindow::~registrationwindow()
{
    delete ui;
}

void registrationwindow::init()
{
    ui->fnameLineEdit->setText("");
    ui->lnameLineEdit->setText("");
    ui->mnameLineEdit->setText("");
    ui->loginLineEdit->setText("");
    ui->passwLineEdit->setText("");
    ui->passNumLineEdit->setText("");
    ui->passSerLineEdit->setText("");
    ui->phoneLineEdit->setText("");
}

void registrationwindow::on_buttonBox_accepted()
{
    QSqlQuery q;
    q.prepare("SELECT * FROM add_client(:f_name, :l_name, :m_name, :pass_ser, :pass_id, :phone, :log, :pas)");
    q.bindValue(":f_name", ui->fnameLineEdit->text());
    q.bindValue(":l_name", ui->lnameLineEdit->text());
    q.bindValue(":m_name", ui->mnameLineEdit->text());
    q.bindValue(":pass_ser", ui->passSerLineEdit->text());
    q.bindValue(":pass_id", ui->passNumLineEdit->text());
    q.bindValue(":phone", ui->phoneLineEdit->text());
    q.bindValue(":log", ui->loginLineEdit->text());
    q.bindValue(":pas", ui->passwLineEdit->text());

    if (!q.exec())
        qDebug() << q.lastError();

    close();
}
