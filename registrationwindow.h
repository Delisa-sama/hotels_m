#ifndef REGISTRATIONWINDOW_H
#define REGISTRATIONWINDOW_H
#include <QtSql>
#include <QDialog>

namespace Ui {
class registrationwindow;
}

class registrationwindow : public QDialog
{
    Q_OBJECT

public:
    explicit registrationwindow(QWidget *parent = 0);
    ~registrationwindow();
    void init();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::registrationwindow *ui;
};

#endif // REGISTRATIONWINDOW_H
