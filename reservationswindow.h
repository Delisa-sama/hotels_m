#ifndef RESERVATIONSWINDOW_H
#define RESERVATIONSWINDOW_H

#include <QWidget>
#include <QtSql>
#include "addreservationdialog.h"

namespace Ui {
class reservationswindow;
}

class reservationswindow : public QWidget
{
    Q_OBJECT

public:
    explicit reservationswindow(QWidget *parent = 0);
    ~reservationswindow();
    void init(QString username, int usernum);

private slots:

    void on_dateBeginEdit_userDateChanged(const QDate &date);

    void on_dateEndEdit_userDateChanged(const QDate &date);

    void on_roomtypesComboBox_currentIndexChanged(const QString &arg1);

    void on_hotelsComboBox_currentIndexChanged(const QString &arg1);

    void on_reservationsTableView_doubleClicked(const QModelIndex &index);

    void on_costFromSpinBox_valueChanged(int arg1);

    void on_costToSpinBox_valueChanged(int arg1);

private:
    Ui::reservationswindow *ui;

    QSqlTableModel *freerooms_m;

    QSqlTableModel *roomtypes_m;
    QSqlTableModel *hotels_m;

    addReservationDialog *addResDialog;

    int user_number;
    QString user_name;

    void update_all();

};

#endif // RESERVATIONSWINDOW_H
