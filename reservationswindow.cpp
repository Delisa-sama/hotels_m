#include "reservationswindow.h"
#include "ui_reservationswindow.h"
#include "QtSql"

void reservationswindow::init(QString username, int usernum)
{
    user_number = usernum;
    user_name = username;

    ui->usernameLabel->setText(username);
}

void reservationswindow::update_all()
{
    freerooms_m->clear();
    freerooms_m->setTable("tmp_free_rooms");
    freerooms_m->setHeaderData(0, Qt::Horizontal, QObject::tr("Отель"));
    freerooms_m->setHeaderData(1, Qt::Horizontal, QObject::tr("Номер комнаты"));
    freerooms_m->setHeaderData(2, Qt::Horizontal, QObject::tr("Тип номера"));
    freerooms_m->setHeaderData(3, Qt::Horizontal, QObject::tr("Стоимость в сутки"));
    if (ui->costFromSpinBox->value() < ui->costToSpinBox->value())
        freerooms_m->setFilter(QString( "_hotel_name='%1'"
                                        " and _type_name='%2'"
                                        " and _cost_per_day >= %3"
                                        " and _cost_per_day <= %4" )
                               .arg(ui->hotelsComboBox->currentText(),
                                    ui->roomtypesComboBox->currentText(),
                                    QString::number(ui->costFromSpinBox->value()),
                                    QString::number(ui->costToSpinBox->value())));
    freerooms_m->select();

    ui->reservationsTableView->hideColumn(4);
    ui->reservationsTableView->hideColumn(5);
    ui->reservationsTableView->hideColumn(6);
    ui->reservationsTableView->hideColumn(7);

    ui->reservationsTableView->verticalHeader()->hide();

    ui->reservationsTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

reservationswindow::reservationswindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::reservationswindow)
{
    ui->setupUi(this);
    QSqlDatabase db = QSqlDatabase::database();

    QSqlQuery q(db);
    q.prepare("SELECT * FROM free_rooms(:date_b, :date_e);");
    q.bindValue("date_b", "'" + ui->dateBeginEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);
    q.bindValue("date_e", "'" + ui->dateEndEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);

    if (!q.exec())
        qDebug() << q.lastError();

    freerooms_m = new QSqlTableModel();
    freerooms_m->setTable("tmp_free_rooms");
    freerooms_m->setHeaderData(0, Qt::Horizontal, QObject::tr("Отель"));
    freerooms_m->setHeaderData(1, Qt::Horizontal, QObject::tr("Номер комнаты"));
    freerooms_m->setHeaderData(2, Qt::Horizontal, QObject::tr("Тип номера"));
    freerooms_m->setHeaderData(3, Qt::Horizontal, QObject::tr("Стоимость в сутки"));
    freerooms_m->select();

    ui->reservationsTableView->setModel(freerooms_m);

    roomtypes_m = new QSqlTableModel(this, db);
    roomtypes_m->setTable("types_of_rooms_v");
    roomtypes_m->setEditStrategy(QSqlTableModel::OnManualSubmit);
    roomtypes_m->select();
    ui->roomtypesComboBox->setModel(roomtypes_m);
    ui->roomtypesComboBox->setModelColumn(1); //Name

    // get ID of selected roomtype
    // QVariant type_number = roomtypes_m->data(roomtypes_m->index(ui->roomtypesComboBox->currentIndex(),0));

    hotels_m = new QSqlTableModel(this, db);
    hotels_m->setTable("hotels_v");
    hotels_m->setEditStrategy(QSqlTableModel::OnManualSubmit);
    hotels_m->select();
    ui->hotelsComboBox->setModel(hotels_m);
    ui->hotelsComboBox->setModelColumn(1); //Name

    addResDialog = new addReservationDialog();

}

reservationswindow::~reservationswindow()
{
    delete ui;
}

void reservationswindow::on_dateBeginEdit_userDateChanged(const QDate &date)
{
    QSqlQuery q(QSqlDatabase::database());
    q.prepare("SELECT * FROM free_rooms(:date_b, :date_e);");
    q.bindValue("date_b", "'" + ui->dateBeginEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);
    q.bindValue("date_e", "'" + ui->dateEndEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);

    if (!q.exec())
        qDebug() << q.lastError();
    freerooms_m->select();
}

void reservationswindow::on_dateEndEdit_userDateChanged(const QDate &date)
{
    QSqlQuery q(QSqlDatabase::database());
    q.prepare("SELECT * FROM free_rooms(:date_b, :date_e);");
    q.bindValue("date_b", "'" + ui->dateBeginEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);
    q.bindValue("date_e", "'" + ui->dateEndEdit->date().toString("yyyy-MM-dd") + "'", QSql::In);

    if (!q.exec())
        qDebug() << q.lastError();
    freerooms_m->select();
}

void reservationswindow::on_roomtypesComboBox_currentIndexChanged(const QString &arg1)
{
    update_all();
}

void reservationswindow::on_hotelsComboBox_currentIndexChanged(const QString &arg1)
{
    update_all();
}

void reservationswindow::on_reservationsTableView_doubleClicked(const QModelIndex &index)
{
    freerooms_m->query().first();
    QModelIndex ind = ui->reservationsTableView->currentIndex();
    QSqlRecord rec = freerooms_m->record(ind.row());

    int room_num = rec.field("_room_number").value().toInt();
    int hotel_num = rec.field("_hotel_num").value().toInt();
    int rooms_count = rec.field("_room_count").value().toInt();
    int persons_count = rec.field("_persons_count").value().toInt();
    int floor = rec.field("_floor_num").value().toInt();
    int cost_pd = rec.field("_cost_per_day").value().toInt();
    QString h_name(rec.field("_hotel_name").value().toString());
    QString r_type(rec.field("_type_name").value().toString());

    addResDialog->init(room_num, hotel_num, rooms_count, persons_count, floor, cost_pd, h_name, r_type, user_number);
    if (addResDialog->exec() == QDialog::Accepted)
        update_all();
}

void reservationswindow::on_costFromSpinBox_valueChanged(int arg1)
{
    update_all();
}

void reservationswindow::on_costToSpinBox_valueChanged(int arg1)
{
    update_all();
}
