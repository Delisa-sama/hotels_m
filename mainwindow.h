#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include "constants.h"
#include "reservationswindow.h"
#include "registrationwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loginButton_clicked();

    void on_registerButton_clicked();

private:
    Ui::MainWindow *ui;
    reservationswindow *res;
    registrationwindow *reg;
    QSqlDatabase db;
    int user_number;
    QString user_name;
};

#endif // MAINWINDOW_H
