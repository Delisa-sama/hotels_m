#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DB_IP "127.0.0.1"
#define DB_PORT 5432
#define DB_NAME "hotels_m"
#define DB_USERNAME "postgres"

#endif // CONSTANTS_H
