#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    reg = new registrationwindow();

    db = QSqlDatabase::addDatabase("QPSQL");
    db.setDatabaseName(DB_NAME);
    db.setUserName(DB_USERNAME);
    db.setHostName(DB_IP);
    db.setPort(DB_PORT);

    db.open();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginButton_clicked()
{

    if (!ui->loginEdit->text().isEmpty())
        db.setUserName(ui->loginEdit->text());
    if (!ui->passwordEdit->text().isEmpty())
        db.setPassword(ui->passwordEdit->text());

    if (!db.open())
        qDebug() << "cannot open connection" << db.lastError();
    else {
        res = new reservationswindow();

        QSqlQuery q;
        q.prepare("SELECT * FROM clients WHERE login=:log");
        q.bindValue(":log", ui->loginEdit->text());

        if (!q.exec())
            qDebug() << q.lastError();

        q.first();
        res->init(ui->loginEdit->text(), q.value("client_number").toInt());
        res->show();
        this->close();
    };// open menu form
}

void MainWindow::on_registerButton_clicked()
{
    reg->init();
    if (reg->exec() == QDialog::Accepted) {
        //

    }
}
