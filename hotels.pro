#-------------------------------------------------
#
# Project created by QtCreator 2018-05-01T14:10:08
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hotels
TEMPLATE = app

INCLUDEPATH += "C:\Users\itach\Desktop\postgresql-10.4-1-windows-binaries\pgsql\include"

LIBS += "-LC:\Users\itach\Desktop\postgresql-10.4-1-windows-binaries\pgsql\bin" -llibeay32 -llibintl-8 -llibpq -lssleay32

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    reservationswindow.cpp \
    addreservationdialog.cpp \
    registrationwindow.cpp

HEADERS += \
        mainwindow.h \
    constants.h \
    reservationswindow.h \
    addreservationdialog.h \
    registrationwindow.h

FORMS += \
        mainwindow.ui \
    reservationswindow.ui \
    addreservationdialog.ui \
    registrationwindow.ui
